package com.example.activityrandomiser.model.base;

import com.example.activityrandomiser.util.DateComparator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "previous_suggestion")
public class PreviousSuggestion {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "suggestion_id")
    private long id;

    @ColumnInfo(name = "datetime")
    private Date dateTime;

    @ColumnInfo(name = "activity_id")
    private long activityId;


    public static PreviousSuggestion of(ActivityAction activity) {
        return new PreviousSuggestion(new Date(), activity.getId());
    }

    public PreviousSuggestion() {
    }

    @Ignore
    public PreviousSuggestion(Date dateTime,long activityId){
        this.dateTime = dateTime;
        this.activityId = activityId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Format the time
     * Show year if its a year not equal to this year
     * @return a formatted time
     */
    public String getFormattedTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getDateTime().getTime());


        //only show day and month if its the same year
        String datePattern = "dd/M/yy";
        String hourPattern = "h:mm a";


        if(DateComparator.isToday(getDateTime())) {
            return new SimpleDateFormat(hourPattern).format(getDateTime());
        } else if(DateComparator.isYesterday(getDateTime())) {
            return "Yesterday";
        }

        return new SimpleDateFormat(datePattern).format(getDateTime());
    }


}
