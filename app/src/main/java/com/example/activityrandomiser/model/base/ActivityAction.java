package com.example.activityrandomiser.model.base;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "activity", indices = @Index("activity_id"))
public class ActivityAction {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "activity_id")
    private long id;

    @ColumnInfo(name = "activity_name")
    private String activityName;

    @ColumnInfo(name = "min_cost")
    private float minCost;

    @ColumnInfo(name = "max_cost")
    private float maxCost;

    @ColumnInfo(name = "min_people")
    private int minPeople;

    @ColumnInfo(name = "max_people")
    private int maxPeople;


    public ActivityAction() {

    }

    @Ignore
    public ActivityAction(String activityName, float minCost, float maxCost, int minPeople, int maxPeople) {
        this.activityName = activityName;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.minPeople = minPeople;
        this.maxPeople = maxPeople;
    }



    private ActivityAction(ActivityBuilder activityBuilder) {
        this.id = activityBuilder.id;
        this.activityName = activityBuilder.activityName;
        this.minCost = activityBuilder.minCost;
        this.maxCost = activityBuilder.maxCost;
        this.minPeople = activityBuilder.minPeople;
        this.maxPeople = activityBuilder.maxPeople;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getActivityName() {
        return activityName;
    }

    public float getMinCost() {
        return minCost;
    }

    public float getMaxCost() {
        return maxCost;
    }

    public int getMinPeople() {
        return minPeople;
    }

    public int getMaxPeople() {
        return maxPeople;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public void setMinCost(float minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(float maxCost) {
        this.maxCost = maxCost;
    }

    public void setMinPeople(int minPeople) {
        this.minPeople = minPeople;
    }

    public void setMaxPeople(int maxPeople) {
        this.maxPeople = maxPeople;
    }

    public static ActivityBuilder builder(long id, String activityName) {
         return new ActivityBuilder(id, activityName);
    }

    /**
     * Use a builder to create the Activity object
     */
    public static class ActivityBuilder {
        long id = 0;
        String activityName;
        int minPeople = 1;
        int maxPeople = 1;
        float maxCost = 0;
        float minCost = 0;

        private ActivityBuilder(long id, String activityName) {
            this.id = id;
            this.activityName = activityName;
        }

        public ActivityBuilder id(long id) {
            this.id = id;
            return this;
        }

        public ActivityBuilder cost(float minCost, float maxCost) {
            this.minCost = minCost;
            this.maxCost = maxCost;
            return this;
        }

        public ActivityBuilder people(int minPeople, int maxPeople) {
            this.minPeople = minPeople;
            this.maxPeople = maxPeople;
            return this;
        }

        public ActivityAction build() {
            return new ActivityAction(this);
        }
    }
}
