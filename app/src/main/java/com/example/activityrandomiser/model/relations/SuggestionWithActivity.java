package com.example.activityrandomiser.model.relations;

import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.PreviousSuggestion;

import androidx.room.Embedded;
import androidx.room.Relation;

public class SuggestionWithActivity {
    @Embedded public PreviousSuggestion previousSuggestion;
    @Relation(
            parentColumn = "activity_id",
            entityColumn = "activity_id",
            entity = ActivityAction.class
    )
    public ActivityAction activity;

    public ActivityAction getActivity() {
        return activity;
    }

    public PreviousSuggestion getPreviousSuggestion() {
        return previousSuggestion;
    }
}
