package com.example.activityrandomiser.model.crossref;

import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName="activity_category",
        primaryKeys = {"category_id", "activity_id"})
public class CategoryActivityCrossRef {
    @ColumnInfo(name = "category_id")
    public long categoryId;
    @ColumnInfo(name = "activity_id", index = true)
    public long activityId;

    public static CategoryActivityCrossRef of(ActivityAction activity, Category category) {
        return new CategoryActivityCrossRef(category.getId(), activity.getId());
    }

    public CategoryActivityCrossRef(long categoryId, long activityId) {
        this.categoryId = categoryId;
        this.activityId = activityId;
    }
}
