package com.example.activityrandomiser.model.base;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "category")
public class Category {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "category_id")
    private long id;

    @ColumnInfo(name = "category_name")
    private String categoryName;

    public Category() {

    }

    @Ignore
    public Category(long id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    @Ignore
    public Category(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getCategoryName() {
        return categoryName;
    }


    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("%s", getCategoryName());
    }
}


