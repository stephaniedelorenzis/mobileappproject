package com.example.activityrandomiser.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.adapter.ActivityRecyclerViewAdapter;
import com.example.activityrandomiser.listener.OnSuggestionClickListener;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;
import com.example.activityrandomiser.viewmodel.PreviousSuggestionViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SuggestionListFragment extends Fragment {
    PreviousSuggestionViewModel viewModel;
    OnSuggestionClickListener suggestionClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.suggestion_list_fragment, container, false);
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.lst_activities);
        if(recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
            final ActivityRecyclerViewAdapter adapter = new ActivityRecyclerViewAdapter(this.suggestionClickListener);
            recyclerView.setAdapter(adapter);

            viewModel = new ViewModelProvider(this).get(PreviousSuggestionViewModel.class);
            viewModel.getAllSuggestions().observe(getViewLifecycleOwner(), new Observer<List<SuggestionWithActivity>>() {
                @Override
                public void onChanged(List<SuggestionWithActivity> suggestionWithActivities) {
                    adapter.setPreviousSuggestions(suggestionWithActivities);
                    if(suggestionWithActivities.size() > 0) {
                        getView().findViewById(R.id.lbl_press_plus).setVisibility(View.INVISIBLE);
                    }
                }
            });

        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, NewSuggestionFragment.instance())
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof OnSuggestionClickListener) {
            this.suggestionClickListener = (OnSuggestionClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.suggestionClickListener = null;
    }

}
