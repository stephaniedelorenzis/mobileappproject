package com.example.activityrandomiser.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.listener.OnSuggestionClickListener;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.ui.fragment.ActivityDetailFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity
        implements OnSuggestionClickListener, ActivityCompat.OnRequestPermissionsResultCallback{

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestLocationPermission();
    }

    @Override
    public void onSuggestionClick(ActivityAction activity) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.nav_host_fragment, ActivityDetailFragment.of(activity))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        return false;
    }

    private void requestLocationPermission() {
        //get location permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //request the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       if(requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
           //create an infinite loop of location permission if user denies it
           //TODO test this well
           if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
               requestLocationPermission();
           }
       }
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        getSupportActionBar().setTitle(titleId);
    }
}
