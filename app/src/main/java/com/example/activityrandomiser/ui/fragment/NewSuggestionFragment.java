package com.example.activityrandomiser.ui.fragment;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.listener.ShakeListener;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.util.Util;
import com.example.activityrandomiser.viewmodel.ActivityRandomiserViewModel;
import com.robinhood.ticker.TickerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public class NewSuggestionFragment extends Fragment {

    private ActivityRandomiserViewModel viewModel;
    private Spinner peopleSpinner;
    private Spinner categorySpinner;
    private TextView txtCost;
    private Button btnDetail;
    private Button btnStart;
    private TickerView tickerView;


    private ActivityAction recommendedAction;
    private SensorManager sensorManager;
    private ShakeListener shakeListener;
    private Category chosenCategory;
    private Integer chosenPeople;
    private double chosenCost;

    private final Handler handler = new Handler();


    public static Fragment instance() {
        return new NewSuggestionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle(R.string.generate_suggestion);
        if(getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        View view = inflater.inflate(R.layout.new_activity_suggestion, container, false);
        Context context = view.getContext();

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        shakeListener = new ShakeListener(this);

        txtCost = view.findViewById(R.id.input_cost);
        tickerView = view.findViewById(R.id.ticker_text);
        btnStart = view.findViewById(R.id.btn_start);
        btnDetail = view.findViewById(R.id.btn_view_detail);
        categorySpinner = view.findViewById(R.id.input_category);
        peopleSpinner = view.findViewById(R.id.input_people);


        //start the generation when the user clicks the start button
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRoll();
            }
        });

        //Show the detail of the chosen suggestion when the button is clicked
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetail();
            }
        });

        viewModel = new ViewModelProvider(this).get(ActivityRandomiserViewModel.class);

        //set the content of the categories and peoples
        final ArrayAdapter<Category> categoryAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryAdapter);

        //add the categories from the database
        viewModel.getAllCategories().observe(getViewLifecycleOwner(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                //reset the data set and notify that its changed
                categoryAdapter.clear();
                categoryAdapter.addAll(categories);
                categoryAdapter.notifyDataSetChanged();
            }
        });

        //Make a list of numbers 1 - 70 for the number of people involved
        List<Integer> peopleCount = new ArrayList();
        int p = 0;
        while(p <= 60) {
            //while the count is less than 20
            if(p < 20) {
                //add incremental value, 1, 2, 3 etc
                peopleCount.add(++p);
            } else {
                //once its over 20 count by tens, 20 30, 40 etc
                peopleCount.add(p += 10);
            }
        }

        ArrayAdapter<Integer> peopleCountAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        peopleCountAdapter.addAll(peopleCount);
        peopleCountAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        peopleSpinner.setAdapter(peopleCountAdapter);

        //setup the ticker
        tickerView.setCharacterLists(Util.getAlphabetString());
        tickerView.setText(getString(R.string.empty), false);
        tickerView.setAnimationDuration(3000);
        tickerView.setPreferredScrollingDirection(TickerView.ScrollingDirection.ANY);
        tickerView.setAnimationInterpolator(new DecelerateInterpolator());

        if(recommendedAction != null) {
            //if we press back we may already have a recommended suggestion
            tickerView.setText(getString(R.string.i_suggest, recommendedAction.getActivityName()), false);
            btnDetail.setEnabled(true);
        }

        return view;
    }

    /**
     * Start the random generation
     */
    public void startRoll() {
        if(categorySpinner.getSelectedItem() instanceof Category) {
            chosenCategory = (Category) categorySpinner.getSelectedItem();
        }

        if(peopleSpinner.getSelectedItem() instanceof Integer) {
            chosenPeople = (Integer) peopleSpinner.getSelectedItem();
        }

        try {
            chosenCost = Double.parseDouble(""+txtCost.getText());
        } catch (NumberFormatException e) {
            //number not entered
        }

        btnDetail.setEnabled(false);
        //add in some dummy text with some random chars to keep its width
        tickerView.setText(getString(R.string.i_suggest, Util.generateRandomString(new Random(), 6)), false);

        /**
         * Run the database query on a separate thread and show the result on the UI thread
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                recommendedAction = viewModel.getRandomSelection(chosenCategory, chosenPeople, chosenCost);

                //when its at max hide the progressbar and show the result
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showResult(recommendedAction);
                    }
                });
            }
        }).start();
    }

    /**
     * Show the result in the view
     * @param suggestion
     */
    private void showResult(ActivityAction suggestion) {
        if(suggestion != null) {
            btnStart.setEnabled(false);
            tickerView.setText(getString(R.string.i_suggest, suggestion.getActivityName()));

            //enable the button once the animation is done
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnDetail.setEnabled(true);
                    btnStart.setEnabled(true);
                    btnStart.setText(R.string.try_again);
                }
            }, tickerView.getAnimationDuration());
        } else {
            btnDetail.setEnabled(false);
            tickerView.setText(getString(R.string.no_result), false);
        }
    }

    /**
     * Open the fragment for the detailed view of the selected suggestion
     */
    public void showDetail() {
        if(this.recommendedAction != null) {
            Fragment nextFragment = ActivityDetailFragment.of(this.recommendedAction);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.nav_host_fragment, nextFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if(accelerometer != null) {
            sensorManager.registerListener(shakeListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onPause() {
        sensorManager.unregisterListener(shakeListener);
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setTitle(R.string.app_name);
    }

}
