package com.example.activityrandomiser.ui.fragment;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.adapter.MarkerInfoAdapter;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.viewmodel.ActivityRandomiserViewModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class ActivityDetailFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnInfoWindowClickListener{

    //bad practice but here for quick use
    private static final String API_KEY = "AIzaSyD-5NMAMkd28X8c7Ekq9VJM8s3ORc7d8qs";
    private static final String KEY_ACTION_ID = "action_id";

    private TextView txtTitle;
    private TextView txtPeople;
    private TextView txtCost;
    private MapView mapView;
    private Button btnShare;
    private GoogleMap mGoogleMap;

    private ActivityRandomiserViewModel viewModel;
    private ActivityAction activityAction;

    /**
     * Create a new fragment that represents the action
     * @param action
     * @return
     */
    public static ActivityDetailFragment of(ActivityAction action) {
        ActivityDetailFragment fragment = new ActivityDetailFragment();
        Bundle args = new Bundle();
        args.putLong(KEY_ACTION_ID, action.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(ActivityRandomiserViewModel.class);
        if(getArguments() != null) {
            activityAction = viewModel.getActivity(getArguments().getLong(KEY_ACTION_ID));
        }

        if(getActivity() instanceof AppCompatActivity){
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggestion_detail, container, false);

        txtTitle = view.findViewById(R.id.txt_activity_title);
        txtPeople = view.findViewById(R.id.txt_people);
        txtCost = view.findViewById(R.id.txt_cost);
        btnShare = view.findViewById(R.id.btn_share);
        mapView = view.findViewById(R.id.mapView);

        //create the map and then init
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        if(activityAction != null) {
            //I know I have an activity so lets startup the map
//            mapView.getMapAsync(this);

            txtTitle.setText(activityAction.getActivityName());
            txtPeople.setText(String.format("%d - %d", activityAction.getMinPeople(), activityAction.getMaxPeople()));
            txtCost.setText(String.format("$%.2f - %.2f", activityAction.getMinCost(), activityAction.getMaxCost()));

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareActivity(view, activityAction);
                }
            });
        }

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //setup all the map options once its loaded
        if (googleMap != null) {
            this.mGoogleMap = googleMap;
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationButtonClickListener(this);
            googleMap.setOnMyLocationClickListener(this);

            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);

            googleMap.setInfoWindowAdapter(new MarkerInfoAdapter(getLayoutInflater()));
            googleMap.setOnInfoWindowClickListener(this);
            getDeviceLocation(googleMap, true);
        }
    }


    /**
     * Add a marker to the map
     * @param prediction
     * @param placesClient
     */
    private void addMarkerForPrediction(final GoogleMap map, AutocompletePrediction prediction, PlacesClient placesClient) {
        if(map != null) {
            String placeId = prediction.getPlaceId();
            List<Place.Field> placeFields = Arrays.asList(Place.Field.NAME, Place.Field.PHONE_NUMBER,
                    Place.Field.OPENING_HOURS, Place.Field.LAT_LNG, Place.Field.WEBSITE_URI, Place.Field.ADDRESS);
            FetchPlaceRequest placeRequest = FetchPlaceRequest.newInstance(placeId, placeFields);
            placesClient.fetchPlace(placeRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                @Override
                public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                    Place place = fetchPlaceResponse.getPlace();
                    map.addMarker(new MarkerOptions().position(place.getLatLng())).setTag(place);
                }
            });
        }
    }


    /**
     * Do a Google Map Search for the places in the user's current area that match the selected activity
     * @param origin
     */
    private void doPlaceSearch(final GoogleMap map, LatLng origin) {
        //do a places search
        Places.initialize(getActivity().getApplicationContext(), API_KEY);
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setQuery(activityAction.getActivityName() + " near me")
                .setSessionToken(token)
                .setCountries("AU")
                .setTypeFilter(TypeFilter.ESTABLISHMENT)
                .setOrigin(origin)
                .setLocationRestriction(RectangularBounds.newInstance(
                        new LatLng(origin.latitude - 0.08, origin.longitude - 0.1),
                        new LatLng(origin.latitude + 0.08, origin.longitude + 0.1)
                ))
                .build();
        if(map != null) {
            final PlacesClient placesClient = Places.createClient(getContext());
            placesClient.findAutocompletePredictions(request).addOnSuccessListener(new OnSuccessListener<FindAutocompletePredictionsResponse>() {
                @Override
                public void onSuccess(FindAutocompletePredictionsResponse findAutocompletePredictionsResponse) {
                    for (AutocompletePrediction prediction : findAutocompletePredictionsResponse.getAutocompletePredictions()) {
                        //add marker to the map
                        addMarkerForPrediction(map, prediction, placesClient);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ApiException) {
                        Log.e("ERROR:", "Place not found: " + e.getMessage());
                    }
                }
            });
        }
    }

    /**
     * Collect the user's current location and use it with the map
     */
    private void getDeviceLocation(final GoogleMap map, final boolean withPlaceSearch) {
        FusedLocationProviderClient client = new FusedLocationProviderClient(getContext());
        Task locationResult = client.getLastLocation();
        locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    Location result = (Location) task.getResult();
                    LatLng origin = new LatLng(result.getLatitude(), result.getLongitude());
                    if(map != null) {
                        //move the camera to an area that is applicable
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(origin, 11));
                    }
                    if(withPlaceSearch) {
                        //Do a search for places that match the selected activity in the user's area
                        doPlaceSearch(map, origin);
                    }


                } else {
                    //it didnt work
                }
            }
        });

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(marker.isInfoWindowShown()) {
            marker.hideInfoWindow();
        } else {
            marker.showInfoWindow();
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        //do nothing (maybe move map closer and redo search
    }


    @Override
    public void onResume() {
        //VERY IMPORTANT - map will load grey without
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    /**
     * Share the activity with a friend
     * This launches a chooser that is applicable to a plain text mime type (ie: textmessage, whatsapp, messenger etc)
     * It will then send a message asking the recipient to try the activity and provide a google map link that
     * searches the selected activity
     * @param view
     */
    public void shareActivity(View view, ActivityAction action) {
        String encodedActivity = Uri.encode(action.getActivityName());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.lets_try,
                action.getActivityName(), "https://google.com/maps/search/?api=1&query=", encodedActivity));
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Activity Idea");
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, getString(R.string.share_details));
        //make sure the user chooses something
        if(sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(shareIntent);
        }
    }

}
