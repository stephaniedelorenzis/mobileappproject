package com.example.activityrandomiser.listener;

import com.example.activityrandomiser.model.base.ActivityAction;

public interface OnSuggestionClickListener {

    /**
     * Define what to do when suggestion is clicked
     * @param activity
     */
    void onSuggestionClick(ActivityAction activity);
}
