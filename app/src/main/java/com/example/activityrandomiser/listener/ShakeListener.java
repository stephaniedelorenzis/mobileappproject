package com.example.activityrandomiser.listener;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.activityrandomiser.ui.fragment.NewSuggestionFragment;

public class ShakeListener implements SensorEventListener {
    private static final int MIN_TIME_BETWEEN_SHAKE = 100000;
    private static final int SHAKE_THRESHOLD = 4;
    private final NewSuggestionFragment fragment;
    private long lastShake;

    public ShakeListener(NewSuggestionFragment activity) {
        this.fragment = activity;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            if((curTime - lastShake) > MIN_TIME_BETWEEN_SHAKE) {

                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                double acceleration = Math.sqrt(Math.pow(x, 2) +
                        Math.pow(y, 2) +
                        Math.pow(z, 2)) - SensorManager.GRAVITY_EARTH;

                if (acceleration > SHAKE_THRESHOLD) {
                    lastShake = curTime;
                    this.fragment.startRoll();
                    Log.d("SHAKE", "Start random choice");
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //do nothing
    }
}
