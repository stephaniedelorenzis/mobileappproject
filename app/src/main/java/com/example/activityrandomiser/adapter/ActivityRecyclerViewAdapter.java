package com.example.activityrandomiser.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.listener.OnSuggestionClickListener;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityRecyclerViewAdapter extends RecyclerView.Adapter<ActivityRecyclerViewAdapter.ViewHolder> {

    static class ViewHolder extends RecyclerView.ViewHolder {

        SuggestionWithActivity selectedItem;
        final View mView;
        final TextView lblActivityName;
        final TextView lblActivityTime;

        ViewHolder(@NonNull View view) {
            super(view);
            mView = view;
            lblActivityName = view.findViewById(R.id.lbl_activity_name);
            lblActivityTime = view.findViewById(R.id.lbl_activity_time);
        }
    }

    private List<SuggestionWithActivity> previousSuggestions;
    private final OnSuggestionClickListener onSuggestionClickListener;

    public ActivityRecyclerViewAdapter(OnSuggestionClickListener onClickListener) {
        this.onSuggestionClickListener = onClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(previousSuggestions != null) {
            final SuggestionWithActivity selectedItem = previousSuggestions.get(position);
            holder.selectedItem = selectedItem;
            if(selectedItem != null) {
                ActivityAction activity = selectedItem.getActivity();
                holder.lblActivityName.setText(activity.getActivityName());
                holder.lblActivityTime.setText(selectedItem.getPreviousSuggestion().getFormattedTime());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (null != onSuggestionClickListener) {
                            onSuggestionClickListener.onSuggestionClick(selectedItem.getActivity());
                        }
                    }
                });
            }

        } else {
            holder.lblActivityName.setText(R.string.no_activity);
        }
    }

    @Override
    public int getItemCount() {
        return this.previousSuggestions == null ? 0 : this.previousSuggestions.size();
    }

    public void setPreviousSuggestions(List<SuggestionWithActivity> suggestions) {
        this.previousSuggestions = suggestions;
        notifyDataSetChanged();
    }
}
