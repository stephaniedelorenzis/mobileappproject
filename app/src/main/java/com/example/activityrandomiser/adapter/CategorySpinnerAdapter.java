package com.example.activityrandomiser.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.activityrandomiser.R;
import com.example.activityrandomiser.model.base.Category;

import java.util.List;

public class CategorySpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final List<Category> categories;
    private final LayoutInflater inflater;

    public CategorySpinnerAdapter(Context context, List<Category> categories){
        this.inflater = LayoutInflater.from(context);
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return this.categories.size();
    }

    @Override
    public Category getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.categories.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(android.R.layout.simple_spinner_item, viewGroup, false);
        }
        Category item = getItem(i);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(item.getCategoryName());
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        Category item = getItem(position);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(item.getCategoryName());
        return view;
    }
}
