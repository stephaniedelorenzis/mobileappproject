package com.example.activityrandomiser.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.activityrandomiser.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.libraries.places.api.model.Place;

public class MarkerInfoAdapter implements GoogleMap.InfoWindowAdapter {

    private final LayoutInflater layoutInflater;

    public MarkerInfoAdapter(LayoutInflater inflater) {
        this.layoutInflater = inflater;
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        if (marker.getTag() instanceof Place) {

            Place place = (Place) marker.getTag();

            View view = getLayoutInflater().inflate(R.layout.marker_layout, null);
            TextView lblTitle = view.findViewById(R.id.marker_title);
            TextView lblPhone = view.findViewById(R.id.marker_txt_phone);
            TextView lblAddress = view.findViewById(R.id.marker_txt_address);
            TextView lblWebsite = view.findViewById(R.id.marker_txt_website);

            String websiteString = place.getWebsiteUri() == null ? "" : place.getWebsiteUri().toString();

            lblTitle.setText(place.getName());
            lblPhone.setText(place.getPhoneNumber());
            lblAddress.setText(place.getAddress());
            lblWebsite.setText(websiteString);
            return view;
        }

        //something went wrong
        Log.e("ERROR", "MarkerInfoAdapter must include the tag Place");
        return null;
    }
}
