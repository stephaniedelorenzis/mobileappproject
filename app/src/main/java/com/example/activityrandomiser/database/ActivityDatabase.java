package com.example.activityrandomiser.database;

import android.content.Context;

import com.example.activityrandomiser.database.converter.Converters;
import com.example.activityrandomiser.database.dao.CategoryActivityDao;
import com.example.activityrandomiser.database.dao.SuggestionDAO;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.model.base.PreviousSuggestion;
import com.example.activityrandomiser.model.crossref.CategoryActivityCrossRef;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {ActivityAction.class, Category.class, PreviousSuggestion.class, CategoryActivityCrossRef.class},
        version = 1, exportSchema = false)
@TypeConverters(Converters.class)
public abstract class ActivityDatabase extends RoomDatabase {

    public abstract SuggestionDAO suggestionDao();
    public abstract CategoryActivityDao categoryActivityDao();

    public static volatile ActivityDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     * lazy instantiation of the database
     * @param context
     * @return
     */
    static ActivityDatabase getDatabase(final Context context) {
        if(INSTANCE == null) {
            synchronized (ActivityDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ActivityDatabase.class, "activity_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static ActivityDatabase.Callback sRoomDatabaseCallback = new ActivityDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);


        }

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    CategoryActivityDao categoryActivityDao = INSTANCE.categoryActivityDao();

                    Category categoryIndoors = new Category(1,"Indoors");
                    categoryActivityDao.insert(categoryIndoors);
                    Category categoryOutdoors = new Category(2,"Outdoors");
                    categoryActivityDao.insert(categoryOutdoors);
                    Category categoryDate = new Category(3,"Date Friendly");
                    categoryActivityDao.insert(categoryDate);
                    Category categoryFood = new Category(4, "Food");
                    categoryActivityDao.insert(categoryFood);
                    Category categoryFamily = new Category(5, "Family Friendly");
                    categoryActivityDao.insert(categoryFamily);
                    Category categoryAdult = new Category(6, "Adults only");
                    categoryActivityDao.insert(categoryAdult);

                    ActivityAction activity =  ActivityAction.builder(1, "Bowling").cost(20, 60).people(2,20).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(2,"Gym").cost(5, 30).people(1, 5).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors));

                    activity = ActivityAction.builder(3,"Movies").cost(15, 25).people(1, 10).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(4,"Paintball").cost(80, 150).people(8, 30).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryOutdoors));

                    activity = ActivityAction.builder(5,"Axe Throwing").cost(50, 70).people(8, 40).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(6, "Dinner Party").cost(0, 50).people(1, 20).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryFood, categoryIndoors, categoryOutdoors, categoryDate));

                    activity = ActivityAction.builder(7, "Netflix and chill").cost(0, 10).people(1, 20).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(8, "Picnic").cost(5, 40).people(1, 100).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryOutdoors, categoryDate));

                    activity = ActivityAction.builder(9, "Chinese Food").cost(10, 40).people(1, 10).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(10, "Murder Mystery Party").cost(80, 120).people(2, 4).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors));

                    activity = ActivityAction.builder(11, "Escape Room").cost(25, 50).people(4, 10).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryIndoors));

                    activity = ActivityAction.builder(12, "Park").cost(0, 20).people(1, 30).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryFamily, categoryDate, categoryOutdoors));

                    activity = ActivityAction.builder(13, "Strippers").cost(40, 100).people(2, 15).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryAdult, categoryIndoors));

                    activity = ActivityAction.builder(14, "High Tea").cost(50, 150).people(2, 10).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryDate, categoryIndoors, categoryFamily));

                    activity = ActivityAction.builder(15, "Pub").cost(5, 100).people(2, 15).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryAdult, categoryIndoors, categoryFood, categoryDate));

                    activity = ActivityAction.builder(16, "Brewery Tour").cost(2, 10).people(2, 5).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryAdult, categoryIndoors, categoryDate));

                    activity = ActivityAction.builder(17, "Museum").cost(10, 20).people(1, 5).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryFamily, categoryIndoors));

                    activity = ActivityAction.builder(18, "Zoo").cost(10, 40).people(1, 5).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryDate, categoryFamily, categoryIndoors, categoryOutdoors));

                    activity = ActivityAction.builder(19, "Aquarium").cost(10, 40).people(1, 5).build();
                    categoryActivityDao.insertActivityWithCategories(activity, Arrays.asList(categoryDate, categoryFamily, categoryIndoors));
                }
            });
        }
    };
}
