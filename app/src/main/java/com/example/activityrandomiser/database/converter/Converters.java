package com.example.activityrandomiser.database.converter;

import java.util.Date;

import androidx.room.TypeConverter;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long dateTime) {
        return dateTime == null ? null : new Date(dateTime);
    }

    @TypeConverter
    public static Long dateToTimestap(Date date) {
        return date == null ? null : date.getTime();
    }
}
