package com.example.activityrandomiser.database.dao;

import com.example.activityrandomiser.model.base.PreviousSuggestion;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public interface SuggestionDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(PreviousSuggestion suggestion);

    @Transaction
    @Query("SELECT * FROM previous_suggestion ORDER BY datetime DESC")
    LiveData<List<SuggestionWithActivity>> getPreviousSuggestions();

    @Query("DELETE FROM previous_suggestion")
    void deleteAll();

    @Transaction
    @Query("SELECT * FROM previous_suggestion WHERE suggestion_id = :id")
    SuggestionWithActivity getSuggestionWithActivity(long id);
}
