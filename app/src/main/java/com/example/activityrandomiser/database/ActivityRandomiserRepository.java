package com.example.activityrandomiser.database;

import android.app.Application;

import com.example.activityrandomiser.database.dao.CategoryActivityDao;
import com.example.activityrandomiser.database.dao.SuggestionDAO;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.model.base.PreviousSuggestion;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;

import java.util.List;
import java.util.concurrent.Callable;

import androidx.lifecycle.LiveData;

public class ActivityRandomiserRepository {

    private final CategoryActivityDao categoryActivityDao;
    private SuggestionDAO suggestionDao;

    private LiveData<List<SuggestionWithActivity>> mAllSuggestions;
    private LiveData<List<Category>> mAllCategories;
    private LiveData<List<ActivityAction>> mAllActivities;

    public ActivityRandomiserRepository(Application application) {
        ActivityDatabase db = ActivityDatabase.getDatabase(application);
        suggestionDao = db.suggestionDao();
        categoryActivityDao = db.categoryActivityDao();

        mAllSuggestions = suggestionDao.getPreviousSuggestions();
        mAllCategories = categoryActivityDao.getAllCategories();
        mAllActivities = categoryActivityDao.getAllActivities();
    }

    public LiveData<List<SuggestionWithActivity>> getAllSuggestions() {
        return mAllSuggestions;
    }

    public void insert(final PreviousSuggestion suggestion) {
        ActivityDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                suggestionDao.insert(suggestion);
            }
        });
    }

    public LiveData<List<Category>> getAllCategories() {
        return mAllCategories;
    }


    public LiveData<List<ActivityAction>> getAllActivities() {
        return mAllActivities;
    }


    /**
     * Return a random suggestion based on the criteria
     * @param chosenCategory
     * @param chosenPeople
     * @param chosenCost
     * @return A Suggestion with Activity attached
     */
    public ActivityAction getRandomSuggestion(final Category chosenCategory, final Integer chosenPeople, final Double chosenCost) {
        return ActivityDatabase.INSTANCE.runInTransaction(new Callable<ActivityAction>() {
            @Override
            public ActivityAction call() {
                //randomly select an activity
                ActivityAction activity = categoryActivityDao.getRandomActivity(chosenCategory.getId(), chosenPeople, chosenCost);
                //create a previous suggestion
                if(activity != null) {
                    PreviousSuggestion suggestion = PreviousSuggestion.of(activity);
                    //insert the new suggestion
                    long id = suggestionDao.insert(suggestion);
                    if (id != -1) {
                        suggestion.setId(id);
                    }
                    //return the newly created suggestion
                    return activity;
                }
                return null;
            }
        });
    }

    public ActivityAction getActivity(final long id) {
//        new AsyncTask<Void, Void, ActivityAction>() {
//
//            @Override
//            protected ActivityAction doInBackground(Void... voids) {
//                return categoryActivityDao.getActivity(id);
//            }
//
//            @Override
//            protected void onPostExecute(ActivityAction activityAction) {
//                super.onPostExecute(activityAction);
//                //do what we want
//            }
//        }.execute();
        return categoryActivityDao.getActivity(id);
    }
}
