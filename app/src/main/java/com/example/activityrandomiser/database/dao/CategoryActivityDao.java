package com.example.activityrandomiser.database.dao;

import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.model.crossref.CategoryActivityCrossRef;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public abstract class CategoryActivityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract long insert(ActivityAction activity);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Category category);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insert(CategoryActivityCrossRef crossRef);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public void insertActivityWithCategories(ActivityAction activity, List<Category> categories) {
        //TODO why does primary KEY not force IGNORE on category/activities
        insert(activity);
        for(Category category : categories) {
            insert(CategoryActivityCrossRef.of(activity, category));
        }
    }

    @Transaction
    @Query("SELECT * FROM activity")
    public abstract LiveData<List<ActivityAction>> getAllActivities();


    @Query("SELECT * FROM category")
    public abstract LiveData<List<Category>> getAllCategories();

    //query to select a random activity that matches our criteria
    @Query("SELECT * FROM activity WHERE activity_id IN (" +
            "SELECT activity.activity_id FROM activity " +
            "INNER JOIN activity_category ON activity_category.activity_id = activity.activity_id " +
            "AND activity_category.category_id = :categoryid " +
            "WHERE :cost >= min_cost " +
            "AND :peopleCount BETWEEN min_people AND max_people " +
            "ORDER BY RANDOM() LIMIT 1)")
    public abstract ActivityAction getRandomActivity(long categoryid, int peopleCount, double cost);

    @Query("SELECT * FROM activity WHERE activity_id = :id")
    public abstract ActivityAction getActivity(long id);

    @Query("DELETE FROM category")
    public abstract void deleteAllCategories();

    @Query("DELETE FROM activity")
    public abstract void deleteAllActivities();
}
