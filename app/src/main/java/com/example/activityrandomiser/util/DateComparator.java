package com.example.activityrandomiser.util;

import java.util.Calendar;
import java.util.Date;

public class DateComparator {

    private DateComparator() {

    }

    /**
     * Compare the days in the calendar
     * @param cal1
     * @param cal2
     * @return
     */
    public static int compareCalendarDay(Calendar cal1, Calendar cal2) {
        return truncateToDay(cal1).compareTo(truncateToDay(cal2));
    }

    /**
     * Determine if the passed in date is today
     * @param date
     * @return true if date is today
     */
    public static boolean isToday(Date date) {
        Calendar calendar = dateToCalendar(date);
        return compareCalendarDay(calendar, Calendar.getInstance()) == 0;
    }

    /**
     * Determine if the date is yesterday
     * @param date
     * @return true if the date is yesterday
     */
    public static boolean isYesterday(Date date) {
        Calendar calendar = dateToCalendar(date);
        Calendar yesterday = Calendar.getInstance();
        yesterday.roll(Calendar.DAY_OF_MONTH, -1);
        return compareCalendarDay(calendar, yesterday) == 0;
    }

    /**
     * Convert a date to a calendar object
     * @param date
     * @return
     */
    private static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    /**
     * Turn a calendar object into an object that just reads the date for comparable reasons
     * @param cal
     * @return
     */
    public static Calendar truncateToDay(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }




}
