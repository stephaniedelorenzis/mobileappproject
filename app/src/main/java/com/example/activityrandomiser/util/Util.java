package com.example.activityrandomiser.util;

import java.util.Random;

public class Util {

    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz ";

    /**
     * Private contructor so you can't instantiate it
     */
    private Util() {

    }

    /**
     * Generte a random string of a length of the provided numbers
     * @param random
     * @param numDigits
     * @return
     */
    public static String generateRandomString(Random random, int numDigits) {
        final char[] result = new char[numDigits];
        for (int i = 0; i < numDigits; i++) {
            result[i] = ALPHABET.charAt(random.nextInt(ALPHABET.length()));
        }
        return new String(result);
    }

    /**
     * Get a string that contains each letter of the alphabet
     * @return
     */
    public static String getAlphabetString() {
        return ALPHABET;
    }

}
