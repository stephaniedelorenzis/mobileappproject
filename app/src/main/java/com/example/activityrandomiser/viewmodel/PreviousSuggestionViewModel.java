package com.example.activityrandomiser.viewmodel;

import android.app.Application;

import com.example.activityrandomiser.database.ActivityRandomiserRepository;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.model.base.PreviousSuggestion;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class PreviousSuggestionViewModel extends AndroidViewModel {
    private ActivityRandomiserRepository mRepository;
    private LiveData<List<SuggestionWithActivity>> mAllSuggestions;
    private LiveData<List<Category>> mAllCategories;
    private LiveData<List<ActivityAction>> mAllActivities;

    public PreviousSuggestionViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ActivityRandomiserRepository(application);

        mAllSuggestions = mRepository.getAllSuggestions();
        mAllCategories = mRepository.getAllCategories();
        mAllActivities = mRepository.getAllActivities();
    }

    public LiveData<List<SuggestionWithActivity>> getAllSuggestions() {
        return mAllSuggestions;
    }

    public void insert(PreviousSuggestion suggestion) {
        mRepository.insert(suggestion);
    }

    public LiveData<List<Category>> getCategories() {
        return mAllCategories;
    }

    public LiveData<List<ActivityAction>> getActivities() {
        return mAllActivities;
    }
}
