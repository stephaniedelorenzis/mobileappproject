package com.example.activityrandomiser.viewmodel;

import android.app.Application;

import com.example.activityrandomiser.database.ActivityRandomiserRepository;
import com.example.activityrandomiser.model.base.ActivityAction;
import com.example.activityrandomiser.model.base.Category;
import com.example.activityrandomiser.model.base.PreviousSuggestion;
import com.example.activityrandomiser.model.relations.SuggestionWithActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ActivityRandomiserViewModel extends AndroidViewModel {
    private ActivityRandomiserRepository mRepository;
    private LiveData<List<SuggestionWithActivity>> mAllSuggestions;
    private LiveData<List<Category>> mAllCategories;


//    private final MediatorLiveData<Long> actionId = new MediatorLiveData<>();
//    private final LiveData<ActivityAction> actionById = Transformations.switchMap(actionId, new Function<Long, LiveData<ActivityAction>>() {
//        @Override
//        public LiveData<ActivityAction> apply(Long input) {
//            return mRepository.getActivity(input);
//        }
//    });

    public ActivityRandomiserViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ActivityRandomiserRepository(application);
        mAllSuggestions = mRepository.getAllSuggestions();
        mAllCategories = mRepository.getAllCategories();
    }

    public LiveData<List<SuggestionWithActivity>> getAllSuggestions() {
        return mAllSuggestions;
    }

    public void insert(PreviousSuggestion suggestion) {
        mRepository.insert(suggestion);
    }

    public LiveData<List<Category>> getAllCategories() {
        return mAllCategories;
    }

//    public Activity getRandomSelection(Category chosenCategory, Integer chosenPeople, Double chosenCost) {
//        return mRepository.getRandomSelection(chosenCategory, chosenPeople, chosenCost);
//    }

    public ActivityAction getRandomSelection(Category chosenCategory, Integer chosenPeople, Double chosenCost) {
        return mRepository.getRandomSuggestion(chosenCategory, chosenPeople, chosenCost);
    }

    /**
     * Get the activity with the provided id from the database
     * @param id
     * @return
     */
    public ActivityAction getActivity(long id) {
        return mRepository.getActivity(id);
    }


//    public LiveData<ActivityAction> getActivityById(long id) {
//        actionId.setValue(id);
//        return actionById;
//    }
}
