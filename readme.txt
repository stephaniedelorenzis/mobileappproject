Student ID: 214181799
Stephanie DeLorenzis

Activity Randomiser
An app to randomly select an activity based on a number of filters.
Keeps track of previous suggestions and allows the user to see details of the suggestion.
In published state but definitely V1. Lots more can be added. 
Includes accelerometer, Google Maps Locations, Room Database, Share intent text/plain

My bitbucket repo:
https://bitbucket.org/stephaniedelorenzis/mobileappproject/src/master/
